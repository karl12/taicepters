package com.karl.taicepters.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.karl.taicepters.TaiCepters;

public class DesktopLauncher {
	public static void main (String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "TaiCepters";
        config.width = 540;
        config.height = 960;
        new LwjglApplication(new TaiCepters(), config);
	}
}
