package com.karl.gameworld;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;
import com.karl.gameobjects.Enemy;
import com.karl.gameobjects.Player;
import com.karl.tchelpers.AssetLoader;
import com.karl.tchelpers.ControlSet;

public class GameRenderer {

	private GameWorld world;
	private ControlSet controlSet;
	
	private OrthographicCamera cam;
	private ShapeRenderer shapeRenderer;

	private SpriteBatch batcher;

	private int height;
	private int width;

	// Objects
	private Player player;
	private ArrayList<Enemy> enemies;

	// Assets
	private TextureRegion ship;
	private TextureRegion arrow;
	
	// Sprites
	private Sprite sprLeftArrow;
	private Sprite sprRightArrow;
	private Sprite sprFireArrow;
	
	private Sprite sprPlayer;

	public GameRenderer(GameWorld world, ControlSet controlSet, int width, int height) {
		this.world = world;
		this.controlSet = controlSet;
		
		this.height = height;
		this.width = width;
		
		cam = new OrthographicCamera();
		cam.setToOrtho(true, width, height);

		batcher = new SpriteBatch();
		batcher.setProjectionMatrix(cam.combined);
		
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setProjectionMatrix(cam.combined);

		// Call helper methods to initialize instance variables
		initGameObjects();
		initAssets();
		initSprites();
	}

	private void initGameObjects() {
		player = world.getPlayer();
		enemies = world.getEnemies();
	}

	private void initAssets() {
		ship = AssetLoader.ship;
		arrow = AssetLoader.arrow;
	}
	
	private void initSprites(){
		sprPlayer = new Sprite(ship);
		sprPlayer.setPosition(player.getPosition().x, player.getPosition().y);
		sprPlayer.setSize(player.getWidth(), player.getHeight());
		
		sprLeftArrow = new Sprite(arrow);
		sprRightArrow = new Sprite(arrow);
		sprFireArrow = new Sprite(arrow);

		
		Circle fire = controlSet.getBtnFire();
		sprFireArrow.setPosition(fire.x, fire.y);

		Circle left = controlSet.getBtnLeft();
		sprLeftArrow.rotate(90);
		sprLeftArrow.setSize(left.radius, left.radius);
		sprLeftArrow.setPosition(left.x, left.y);
		
		
		
		Circle right = controlSet.getBtnRight();
		sprRightArrow.rotate(270);
		sprRightArrow.setPosition(right.x, right.y);		

	}

	private void drawControlSet(){
		
		sprFireArrow.draw(batcher);
		sprRightArrow.draw(batcher);
		sprLeftArrow.draw(batcher);
		
	}
	
	public void render(float runTime) {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		shapeRenderer.begin(ShapeType.Filled);

		// Draw Background color
		shapeRenderer.setColor(0 / 255.0f, 0 / 255.0f, 0 / 255.0f, 1);
		shapeRenderer.rect(0, 0, width, height);
		


		shapeRenderer.end();

		batcher.begin();

		sprPlayer.setPosition(player.getPosition().x, player.getPosition().y);
		sprPlayer.draw(batcher);
		
		drawControlSet();
		
		batcher.end();
		

		shapeRenderer.begin(ShapeType.Line);

		shapeRenderer.setColor(0 / 255.0f, 255 / 255.0f, 0 / 255.0f, 1);
		
		shapeRenderer.circle(controlSet.getBtnLeft().x, controlSet.getBtnLeft().y, controlSet.getBtnLeft().radius);
		shapeRenderer.circle(controlSet.getBtnRight().x, controlSet.getBtnRight().y, controlSet.getBtnRight().radius);
		shapeRenderer.circle(controlSet.getBtnFire().x, controlSet.getBtnFire().y, controlSet.getBtnFire().radius);
		

		shapeRenderer.end();
	}
}
