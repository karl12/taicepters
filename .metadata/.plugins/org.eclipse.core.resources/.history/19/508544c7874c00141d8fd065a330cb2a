package com.karl.gameworld;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Intersector;
import com.karl.gameobjects.EnemySquad;
import com.karl.gameobjects.Player;
import com.karl.gameobjects.Projectile;
import com.karl.gameobjects.Ship;

public class GameWorld {

	private Player player;
	private EnemySquad enemySquad;

	public GameWorld(int screenWidth, int screenHeight) {
		player = new Player((screenWidth / 2) - (75 / 2), screenHeight - 150,
				75, 116, screenWidth, screenHeight);

		enemySquad = new EnemySquad(5, 8, screenWidth, screenHeight);
	}

	public void update(float delta) {
		player.update(delta);
		enemySquad.update(delta);

		updateProjectiles(player, delta);

		for (int i = 0; i < enemySquad.getRows(); i++) {
			for (int j = 0; j < enemySquad.getColumns(i); j++) {
				updateProjectiles(enemySquad.getEnemy(i, j), delta);
			}
		}
	}

	public Player getPlayer() {
		return player;
	}

	public EnemySquad getEnemySquad() {
		return enemySquad;
	}

	private void updateProjectiles(Ship ship, float delta) {
		ArrayList<Projectile> projectiles = ship.getProjectiles();
		for (int i = 0; i < projectiles.size(); i++) {
			Projectile p = projectiles.get(i);
			if (p.isVisible()) {
				p.update(delta);
				detectHit(p);
			} else if (!p.isVisible()) {
				projectiles.remove(i);
			}
		}
	}

	private void detectHit(Projectile p) {
		for (int i = 0; i < enemySquad.getRows(); i++) {
			for (int j = 0; j < enemySquad.getColumns(i); j++) {
				if(Intersector.overlaps(enemySquad.getEnemy(i, j).getHitCircle(), p.getHitRect())){
					p.setVisible(false);
					enemySquad.removeEnemy(i, j);
				}
			}
		}
	}
}