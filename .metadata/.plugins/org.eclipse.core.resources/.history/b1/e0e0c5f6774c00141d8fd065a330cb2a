package com.karl.gameobjects;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public abstract class Ship {

	protected Vector2 position;
	protected Vector2 velocity;
	
	protected int orientation; //1 down -1 up  -  !!Must be set by subclass!!
	
	protected ArrayList<Projectile> projectiles;
	
	protected int width;
	private int height;
	
	public ArrayList<Projectile> getProjectiles() {
		return projectiles;
	}

	public void setProjectiles(ArrayList<Projectile> projectiles) {
		this.projectiles = projectiles;
	}

	protected int screenHeight;
	protected int screenWidth;
	
	public Ship(float x, float y, int width, int height,
			int screenWidth, int screenHeight){
		this.width = width;
		this.height = height;
		this.screenHeight = screenHeight;
		this.screenWidth = screenWidth;
		
		orientation = 1;
		
		position = new Vector2(x, y);
		velocity = new Vector2(0, 0);
		
		projectiles = new ArrayList<Projectile>();
	}
	
	public abstract void update(float delta);
	
	public void shoot(){
		Vector2 pPosition = position.cpy();
		
		if (orientation == 1){
			pPosition.add(width/2, height);
		}else if (orientation == -1){
			pPosition.add(width/2, 0);
		}
		
		Projectile p = new Projectile(pPosition, screenHeight, orientation);
		projectiles.add(p);
	}
	
	//!!must be added to subclasses update!!
	protected void updateProjectiles(float delta){
		for(int i = 0; i < projectiles.size(); i++){
			Projectile p = projectiles.get(i);
			if (p.isVisible()){
				p.update(delta);
			}else if (!p.isVisible()){
				projectiles.remove(i);
			}
		}
	}

	public Vector2 getPosition() {
		return position;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}
}
