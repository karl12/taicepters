package com.karl.gameworld;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;
import com.karl.gameobjects.EnemySquad;
import com.karl.gameobjects.Player;
import com.karl.gameobjects.Projectile;
import com.karl.tchelpers.AssetLoader;
import com.karl.tcui.ControlSet;

public class GameRenderer {

	private GameWorld world;
	private ControlSet controlSet;
	
	private OrthographicCamera cam;
	private ShapeRenderer shapeRenderer;

	private SpriteBatch batcher;

	private int height;
	private int width;

	// Objects
	private Player player;
	private EnemySquad enemySquad;
	
	// Assets
	private TextureRegion ship;
	private TextureRegion arrow;
	private TextureRegion enemy;
	
	// Sprites
	private Sprite sprLeftArrow;
	private Sprite sprRightArrow;
	private Sprite sprFireArrow;
	
	private Sprite sprPlayer;
	
	private Sprite sprEnemy;

	public GameRenderer(GameWorld world, ControlSet controlSet, int width, int height) {
		this.world = world;
		this.controlSet = controlSet;
		
		this.height = height;
		this.width = width;
		
		cam = new OrthographicCamera();
		cam.setToOrtho(true, width, height);

		batcher = new SpriteBatch();
		batcher.setProjectionMatrix(cam.combined);
		
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setProjectionMatrix(cam.combined);

		// Call helper methods to initialize instance variables
		initGameObjects();
		initAssets();
		initSprites();
	}

	private void initGameObjects() {
		player = world.getPlayer();
		enemySquad = world.getEnemySquad();
	}

	private void initAssets() {
		ship = AssetLoader.ship;
		arrow = AssetLoader.arrow;
		enemy = AssetLoader.enemy;
	}
	
	private void initSprites(){
		sprPlayer = new Sprite(ship);
		sprPlayer.setPosition(player.getPosition().x, player.getPosition().y);
		sprPlayer.setSize(player.getWidth(), player.getHeight());
		
		sprEnemy = new Sprite(enemy);
		sprEnemy.setSize(enemySquad.getEnemy(0, 0).getWidth(),
				enemySquad.getEnemy(0, 0).getHeight());
		
		sprLeftArrow = new Sprite(arrow);
		sprRightArrow = new Sprite(arrow);
		sprFireArrow = new Sprite(arrow);

		
		Circle fire = controlSet.getBtnFire();
		sprFireArrow.setSize(fire.radius*2, fire.radius*2);
		sprFireArrow.setPosition(fire.x - fire.radius, fire.y - fire.radius);

		Circle left = controlSet.getBtnLeft();
		sprLeftArrow.rotate(270);
		sprLeftArrow.setSize(left.radius*2, left.radius*2);
		sprLeftArrow.setPosition(left.x - left.radius, left.y - left.radius);
		
		
		Circle right = controlSet.getBtnRight();
		sprRightArrow.rotate(90);
		sprRightArrow.setSize(right.radius*2, right.radius*2);
		sprRightArrow.setPosition(right.x - right.radius, right.y - right.radius);		

	}

	private void drawControlSet(){
		sprFireArrow.draw(batcher);
		sprRightArrow.draw(batcher);
		sprLeftArrow.draw(batcher);	
	}
	
	private void drawEnemySquad(){
		for(int i = 0; i< enemySquad.getRows(); i++){
			for(int j = 0; j < enemySquad.getColumns(); j++){
				sprEnemy.setPosition(enemySquad.getEnemy(i, j).getPosition().x,
						enemySquad.getEnemy(i, j).getPosition().y);
			}
		}
	}
	
	public void render(float runTime) {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		shapeRenderer.begin(ShapeType.Filled);

		// Draw Background color
		shapeRenderer.setColor(0 / 255.0f, 0 / 255.0f, 0 / 255.0f, 1);
		shapeRenderer.rect(0, 0, width, height);

		shapeRenderer.setColor(Color.RED);
		ArrayList<Projectile> projectiles = player.getProjectiles();
		for (int i = 0; i < projectiles.size(); i++){
			Projectile p = projectiles.get(i);
			shapeRenderer.rect(p.getPosition().x, p.getPosition().y, 5, 20);
		}
		
		shapeRenderer.end();

		batcher.begin();

		sprPlayer.setPosition(player.getPosition().x, player.getPosition().y);
		sprPlayer.draw(batcher);
		
		drawControlSet();
		
		batcher.end();
		

		shapeRenderer.begin(ShapeType.Line);

		shapeRenderer.setColor(0 / 255.0f, 255 / 255.0f, 0 / 255.0f, 1);
		
		shapeRenderer.circle(controlSet.getBtnLeft().x, controlSet.getBtnLeft().y, controlSet.getBtnLeft().radius);
		shapeRenderer.circle(controlSet.getBtnRight().x, controlSet.getBtnRight().y, controlSet.getBtnRight().radius);
		shapeRenderer.circle(controlSet.getBtnFire().x, controlSet.getBtnFire().y, controlSet.getBtnFire().radius);
		shapeRenderer.rect(player.getPosition().x, player.getPosition().y, player.getWidth(), player.getHeight());

		shapeRenderer.end();
	}
}
