package com.karl.taicepters;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.karl.screens.GameScreen;
import com.karl.tchelpers.AssetLoader;

public class TaiCepters extends Game {

	GameScreen gameScreen;
	
	@Override
	public void create() {
		Gdx.app.log("TaiCepters", "created");
		AssetLoader.load();
		
		gameScreen = new GameScreen(this);
		setScreen(gameScreen);
	}
	
    @Override
    public void dispose() {
        super.dispose();
        AssetLoader.dispose();
    }
}