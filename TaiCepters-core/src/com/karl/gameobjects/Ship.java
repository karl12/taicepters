package com.karl.gameobjects;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public abstract class Ship {

	protected Vector2 position;
	protected Vector2 velocity;
	
	protected int health;
	
	protected int orientation; //1 down -1 up  -  !!Must be set by subclass!!
	
	public int getOrientation() {
		return orientation;
	}

	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}

	protected ArrayList<Projectile> projectiles;
	
	protected int width;
	protected int height;
	
	public ArrayList<Projectile> getProjectiles() {
		return projectiles;
	}

	public void setProjectiles(ArrayList<Projectile> projectiles) {
		this.projectiles = projectiles;
	}

	protected int screenHeight;
	protected int screenWidth;
	
	public Ship(float x, float y, int width, int height,
			int screenWidth, int screenHeight){
		this.width = width;
		this.height = height;
		this.screenHeight = screenHeight;
		this.screenWidth = screenWidth;
		
		orientation = 1;
		
		setHealth(5);
		
		position = new Vector2(x, y);
		velocity = new Vector2(0, 0);
		
		projectiles = new ArrayList<Projectile>();
	}
	
	public abstract void update(float delta);
	
	public void shoot(){
		Vector2 pPosition = position.cpy();
		
		if (orientation == 1){
			pPosition.add(width/2, height);
		}else if (orientation == -1){
			pPosition.add(width/2, 0);
		}
		
		Projectile p = new Projectile(pPosition, screenHeight, orientation);
		projectiles.add(p);
	}
	
	public Vector2 getPosition() {
		return position;
	}

	public Vector2 getVelocity() {
		return velocity;
	}
	
	public void hit(){
		setHealth(getHealth() - 1);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}
}
