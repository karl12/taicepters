package com.karl.gameobjects;

import com.badlogic.gdx.math.Circle;

public class Enemy extends Ship {

	private boolean isMovingDown = false;
	private int downwardMovement;
	private int column;
	private boolean isAtFront;

	private Circle hitCircle;

	public Enemy(float x, float y, int width, int height, int screenWidth,
			int screenHeight, int column) {
		super(x, y, width, height, screenWidth, screenHeight);
		
		orientation = 1;

		this.column = column;
		this.setAtFront(isAtFront);
		
		velocity.x = 50;

		setHitCircle(new Circle(position.x, position.y, width / 2));
		
		health = 2;
	}

	@Override
	public void update(float delta) {
		int previousY = (int) position.y;
		position.add(velocity.cpy().scl(delta));
		hitCircle.setPosition(position);

		if (isMovingDown) {
			downwardMovement += position.y - previousY;
			if (downwardMovement > height / 2) {
				velocity.y = 0;
				isMovingDown = false;
				downwardMovement = 0;
			}
		}
		
		if(isAtFront &&
				Math.random() > 0.995){
			shoot();
		}
	}

	public void hitSide() {
		velocity.x *= -1;
		moveDown();
	}

	public boolean isMovingDown() {
		return isMovingDown;
	}

	private void moveDown() {
		isMovingDown = true;
		velocity.y += 100;
	}

	public Circle getHitCircle() {
		return hitCircle;
	}

	public void setHitCircle(Circle hitCircle) {
		this.hitCircle = hitCircle;
	}

	public boolean isAtFront() {
		return isAtFront;
	}

	public void setAtFront(boolean isAtFront) {
		this.isAtFront = isAtFront;
	}

	public int getColumn() {
		return column;
	}

}
