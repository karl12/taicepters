package com.karl.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Projectile {

	private Vector2 position;
	private Vector2 velocity;
	
	private int screenHeight;
	
	private boolean visible;
	
	private Rectangle hitRect;
	
	public Projectile(Vector2 position, int screenHeight, int orientation){
		this.position = position;
		this.screenHeight = screenHeight;
		
		int sizeRatio = screenHeight/20;
		
		hitRect = new Rectangle(position.x, position.y, sizeRatio/2, sizeRatio);
		
		velocity = new Vector2();
		velocity.y = 600 * orientation;
		
		visible = true;
	}

	public void update(float delta){
		position.add(velocity.cpy().scl(delta));
		hitRect.setPosition(position.x, position.y);
		if(position.y < 0 || position.y > screenHeight){
			visible = false;
		}
	}
	
	public Rectangle getHitRect() {
		return hitRect;
	}

	public void setHitRect(Rectangle hitRect) {
		this.hitRect = hitRect;
	}

	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
