package com.karl.tchelpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AssetLoader {
	
	public static Texture textureShip, textureArrow, textureEnemy;
	public static TextureRegion ship, arrow, enemy;
	
	public static BitmapFont largeFont;
	public static BitmapFont mediumFont;
	public static BitmapFont smallFont;
	
	public static void load(){
        textureShip = new Texture(Gdx.files.internal("data/texture.png"));
        textureShip.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        ship = new TextureRegion(textureShip, 0, 0, 376, 582);
        ship.flip(false, true);
        
        textureEnemy = new Texture(Gdx.files.internal("data/spaceship.png"));
        textureEnemy.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        enemy = new TextureRegion(textureEnemy, 0, 0, 523, 484);
        enemy.flip(false, true);
        
        textureArrow = new Texture(Gdx.files.internal("data/arrow.png"));
        textureArrow.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        arrow = new TextureRegion(textureArrow, 0, 0, 64, 64);
        
        largeFont = new BitmapFont(Gdx.files.internal("data/text.fnt"));
        largeFont.setScale(1f, -1f);
        
        smallFont = new BitmapFont(Gdx.files.internal("data/text.fnt"));
        smallFont.setScale(0.25f, -0.25f);
        
        mediumFont = new BitmapFont(Gdx.files.internal("data/text.fnt"));
        mediumFont.setScale(0.5f, -0.5f);
        
	}
	
	public static void dispose(){
		textureShip.dispose();
		textureArrow.dispose();
		textureEnemy.dispose();
		
		largeFont.dispose();
		mediumFont.dispose();
		smallFont.dispose();

	}
}
