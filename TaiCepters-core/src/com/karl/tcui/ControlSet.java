package com.karl.tcui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

public class ControlSet {

	private Circle btnLeft;
	private Circle btnRight;
	private Circle btnFire;
	
	public ControlSet(int width, int height, int radius){
		
		//set position relative to screen size
		Vector2 right = new Vector2();
		right.x = (int) (width * 0.85);
		right.y = (int) (height * 0.9);
		
		Vector2 left = new Vector2();
		left.x = (int) (width * 0.6);
		left.y = (int) (height * 0.9);
		
		Vector2 fire = new Vector2();
		fire.x = (int) (width * 0.1);
		fire.y = (int) (height * 0.9);
		
		//init buttons
		btnLeft = new Circle(left, radius);
		btnRight = new Circle(right, radius);
		btnFire = new Circle(fire, radius);
		
	}

	public Circle getBtnLeft() {
		return btnLeft;
	}

	public Circle getBtnRight() {
		return btnRight;
	}

	public Circle getBtnFire() {
		return btnFire;
	}
}
